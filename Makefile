.PHONY: all

all: tree.neut.native test.lean.native

test.lean.native: test.lean.c
	leanc -O3 -o test.lean.native test.lean.c

test.lean.c: test.lean
	lean --c test.lean.c test.lean

tree.ll: target/artifact/tree.ll
	sed 's/@main()/@main_neut()/' < $< > $@

target/artifact/tree.ll: source/tree.nt
	neut build --emit llvm --skip-link tree

tree.neut.native: tree.ll tree_driver.c
	clang -O3 -o tree.neut.native tree_driver.c tree.ll
