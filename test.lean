inductive Tree where
| mk (left: Option Tree) (right : Option Tree) : Tree

def Tree.balanced_with_depth : (n: Nat) -> Tree
| 0 => .mk .none .none
| .succ n => .mk (balanced_with_depth n) (balanced_with_depth n)

def Tree.check : Option Tree -> Nat
| .none => 0
| .some ⟨left,right⟩ => 1 + check left + check right

def main : IO Unit := do
  let t := Tree.balanced_with_depth 30
  IO.println <| Tree.check <| .some t
